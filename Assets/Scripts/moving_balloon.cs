﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moving_balloon : MonoBehaviour
{
    private Vector3 _originalPosition;

    public float waveHeight = 1f;

    private void Awake()
    {
        _originalPosition = transform.position;
    }

    public float maxSpeed = 6.0f;


    // Start is called before the first frame update
    private void Update()
    {
        transform.localPosition = Vector3.up * waveHeight * 0.5f * (0.5f + Mathf.Sin(Time.time * maxSpeed)) + _originalPosition;
    }
}