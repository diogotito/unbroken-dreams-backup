﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBarSprite : MonoBehaviour
{
    public GameObject bar;
    public Part part;
    SpriteRenderer[] spriteRenderers;

    // Start is called before the first frame update
    void Start()
    {
        this.spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!part || !part.isActiveAndEnabled) {
            
            foreach(var t in spriteRenderers) {
                t.enabled = false;
            }
        }
        else
        {
            foreach (var t in spriteRenderers)
            {
                t.enabled = true;
            }

            bar.transform.localScale = new Vector3(part.currentHp / part.totalHp, 1, 1);
            gameObject.SetActive(true);
        }
    }
}
