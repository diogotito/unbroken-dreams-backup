﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class DialogueManager : MonoBehaviour
{
	/*
	 * OK BOYS LISTEN UP
	 * how is this all going to roll? Well i'll tell you, buckle up kangaroo for this is a tale of spice and everything nice, these are the ingredients to create the perfect little girls, scratch that, the perfect lolis
	 * 
	 * So when the character collides with X character, it will create a ballon, that part is created in the Dialog script which is on the Doll,
	 * If the player clicks on the talk key (dunno which will be) that will fire an event, the start_conversation and it'll be called with the name of the character that the doll collided with
	 * 
	 * When that happens we come to the DialogueManager (this script) and fire up the start_converstation(string name) and this will happen
	 * 1 º the background goes a little black with an animation
	 * 2 º a white (or other collor) box will appear on the bottom of the screen (animated of course)
	 * 3 º an image with the characters (both the doll and the character the doll collided with), will appear on the screen with an animation, the doll from the left to the right and the other one from the right to the left of the screen
	 * 4 º a coroutine will start with the desired talk.
	 * 
	 * Now you might ask how is the talk controlled? Well the characters will have a default talk and an index talk
	 * That will be controlled by having an index counter for each character and a bool that represents the default talk.
	 *
	 * NOTE: it's very important that the variables and coroutines names start with the name of the colliders to automate this process, in a way instead of creating multiple checks it'll just call the name followed by an _ with the rest of the required text
	 */

	// This are the index talks that will control what is to be said for each character
	private int BBEG_img_index = 0;
	private int Robot_img_index = 0;
	private int Dino_img_index = 0;
	private int Car_img_index = 0;

	// This will be the controller for the default talks
	bool BBEG_img_default = false;
	bool Robot_img_default = false;
	bool Dino_img_default = false;
	bool Car_img_default = false;

	//for the background
	Image dial_background;
	Image Doll_img;
	Image Collider_img;

	//TEMPORARIAMENTE
	public string name_fight;

	//this is for the images the characters will have
	//NOTE: THE FIRST POSITION IS FOR THE DOLL FACE, SO NO ONE CHANGE IT characters_to_change[0]!!!!!
	public Sprite[] Characters_to_change;

	//This is for the start pos of the both doll image and character image
	float init_doll_char_img = 1700;
	float endi_doll_char_img = 680;


	//THIS IS THE dictionary that will keep all the talks
	Dictionary<string, List<(string, string)>> the_talks;

	//this is for the bottom chat window
	Image bottom_chat_window;

	//THIS IS FOR THE dialogue and name elements

	public TMP_Text dialogue_chat;
	public TMP_Text dialogue_name;


	//TO DELETE LATERRRR ON THIS IS JUST FOR NOW!!!
	public Enemy robot;
	public Enemy urso;
	public Enemy dino;
	public Enemy car;

    public GameObject robot_img;
    public GameObject urso_img;
    public GameObject dino_img;
    public GameObject car_img;

    private void Awake()
	{
		//This gets the image black background
		dial_background = GameObject.Find("dialogue_background").GetComponent<Image>();

		//this is for the doll image that will be default
		Doll_img = GameObject.Find("Doll_img").GetComponent<Image>();

		//now this image will be changed a lot, it'll depend on the guy who's colliding with the doll
		Collider_img = GameObject.Find("Collider_img").GetComponent<Image>();


		//first thing's first, we set the background alpha to 0, unless we don't want to see anything but black *wink wink*
		dial_background.canvasRenderer.SetAlpha(0.0f);

		//make sure the images are outside the visual mapping
		Doll_img.rectTransform.localPosition = new Vector3(init_doll_char_img, Doll_img.rectTransform.localPosition.y, Doll_img.rectTransform.localPosition.z);
		Collider_img.rectTransform.localPosition = new Vector3(-init_doll_char_img, Collider_img.rectTransform.localPosition.y, Collider_img.rectTransform.localPosition.z);

		//To detect the bottom dialogue background
		bottom_chat_window = GameObject.Find("dialogue_window").GetComponent<Image>();


		//THE CONSTRUCT FOR THE TALKS
		the_talks = new Dictionary<string, List<(string, string)>>()
		{
			[ "BBEG_img"] = new List<(string, string)>{
					("Teddy", "I see you've changed, I like the way you handle things around"),
					("Isabel", "If I want to have revenge I need to get stronger and stronger parts!"),
					("Teddy", "You're not the only one who has become stronger"),
					("final_fight", "for the lube"),
					("Teddy", "Observe your final test!"),
					("Isabel", "You're in my way and you'll regret it!")
				},

			["Good_teady_img"] = new List<(string, string)>{
					/*("Teddy", "So, I see you also have been left behind"),
					("Isabel", "..."),
					("Teddy", "You're broken, you're nothing but pieces now"),
					("Isabel", "..."),
					("Teddy", "Let me see if i can repair you"),
					("intro_1", "to the glue"),
					("Teddy", "Here, some glue to attend your broken parts"),
					("intro_2", "for the lube"),
					("Teddy", "There's also some spray that will boost your parts if you find them"),*/
					("Isabel", "... HUMP *cof* "),
					("Teddy", "So she can talk, some improvements indeed"),
					("Isabel", "Where am I? Who are you? Where's the human?"),
					("Teddy", "You're confused so I'll give you a brief explanation"),
					("Teddy", "You're in the depts of the girl's room, there's nothing but scraps and pieces of old toys around"),
					("Isabel", "I... want... REVENGE!"),
				},

			["Robot_img"] = new List<(string, string)>{
					("Robot", "*BOOP* *BEEP* Uniteligente life form detected"),
					("Isabel", "Coming from someone who's dripping oil"),
					("Robot", "*BEEP* I am the ultimate life form, you're no match for my laser arms"),
					("Isabel", "Your laser arms will be a good addition to my collection"),
					("Robot", "*BEEP BEEP* Danger Danger, activating fighting sequences"),
					("Robot", "EXTERMINATE!"),
				},

			["Dino_img"] = new List<(string, string)>{
					("Dino", "Well well well, look what the table brought us"),
					("Dino", "A broken little doll, how sweet"),
					("Isabel", "My name is Isabel and your claws look sharp."),
					("Isabel", "Would you please care to share one of them with me?"),
					("Dino", "I'm sure my claws will cut you like the little porcelain you are"),
					("Isabel", "I'll enjoy tearing your little reptile limbs appart."),
				},

			["Car_img"] = new List<(string, string)>{
					("Isabel", "*Tring Tring* Oh look there's a call for you"),
					("Car", "Is The Owner who doesn't want to play with you anymore"),
					("Isabel", "I was planning going easy on you, but you made me change my mind"),
					("Car", "You're too slow for my fast hot wheels"),
					("Isabel", "We'll see about that"),
				}
		};


		start_conversation("Good_teady_img");

		//THIS ALSO WORKS
		/*
		the_talks = new Dictionary<string, List<(string, string)>>()
		{
			{"BBEG_img", new List<(string, string)>{
					("teste", "teste"),
				}
			}
		};
		*/

		//THIS WORKS
		/*the_talks.Add("BBEG_img", new List<(string, string)> { 
			("teste", "out retsd")});
		*/
	}

	public void start_conversation(string name)
	{

		//TO DELL LATER ON
		name_fight = name;

		/*
		 * The first steps here are
		 * 1º put the background with a black color with the alpha of .4
		 * 2º at the same time the white box appears, make both characters appear
		 * 3º start the conversation with the coroutine
		 */

		//ok first we set the cross fade to be around .4 with the speed of .3, that way it'll feel like a smooth transition for the talk part
		dial_background.CrossFadeAlpha(0.4f, .3f, false);

		//set the doll_img sprite to the doll_sprite
		Doll_img.sprite = Characters_to_change[0];

		//now we need to see what's the character we're colliding and then putt the correct image to the collider_img
		switch (name)
		{
			case "BBEG_img":
				Collider_img.sprite = Characters_to_change[5];
				break;
			case "Robot_img":
				Collider_img.sprite = Characters_to_change[2];
				break;
			case "Dino_img":
				Collider_img.sprite = Characters_to_change[3];
				break;
			case "Car_img":
				Collider_img.sprite = Characters_to_change[4];
				break;
			case "Good_teady_img":
				Collider_img.sprite = Characters_to_change[5];
				break;
		}

		//after that we need to make them appear (we could unactivate them and then make them tween, just for the lulz)

	
		Doll_img.rectTransform.DOLocalMoveX(-endi_doll_char_img, 1.0f);
		Collider_img.rectTransform.DOLocalMoveX(endi_doll_char_img, 1.0f);
		bottom_chat_window.rectTransform.DOLocalMoveY(-487, 1.0f).onComplete += () => StartCoroutine(Drive_Conversation(name_fight));

	}



	IEnumerator Drive_Conversation(string talkz)
	{
		foreach ((string chara,string line) in the_talks[talkz])
		{
			Debug.Log(chara);
			if( chara.Equals("Teddy")  || 
				chara.Equals("Isabel") ||
				chara.Equals("Dino")   ||
				chara.Equals("Car")    ||
				chara.Equals("Robot"))
			{
				dialogue_name.text = chara;
				dialogue_chat.text = line;
				yield return new WaitUntil(() => Input.GetMouseButtonUp(0));
				yield return new WaitForSeconds(0.05f);
			}
			else
			{
				switch (chara)
				{
					case "intro_1":

						break;
					case "intro_2":
						break;
					case "final_fight":
						Collider_img.sprite = Characters_to_change[1];
						yield return new WaitForSeconds(1.0f);
						break;
					default:
						break;
				}
			}
		}

		end_conversation();
	}

	void end_conversation()
	{
		dial_background.CrossFadeAlpha(0.0f, .3f, false);
		Doll_img.rectTransform.DOLocalMoveX(init_doll_char_img, 1.0f);
		Collider_img.rectTransform.DOLocalMoveX(-init_doll_char_img, 1.0f);
		bottom_chat_window.rectTransform.DOLocalMoveY(-900, 1.0f).onComplete += start_combat;

		dialogue_name.text = "";
		dialogue_chat.text = "";


	}

	void start_combat()
	{
		//PARA ALTERAR DEPOIS ISTO TUDOOOO
		GameManager pl = GameObject.Find("GameManager").GetComponent<GameManager>();
		switch (name_fight){
			case "BBEG_img":
				pl.startCombat(urso);
                Destroy(this.urso_img);
                break;
			case "Robot_img":
				pl.startCombat(robot);
                Destroy(this.robot_img);
                break;
			case "Dino_img":
				pl.startCombat(dino);
                Destroy(this.dino_img);
                break;
			case "Car_img":
				pl.startCombat(car);
                Destroy(this.car_img);
                break;
			case "Good_teady_img":
				break;
		}
	}
}
