﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class PartHoveredEvent : UnityEvent<Part>
{
    // OMFG ESTES EVENTOS SAO DEFICIENTES PK EH QUE UNITYEVENT<T0> EH ABSTRACT DEVIA TER USADO DELEGATES E EVENTS DO C#
}

public class PartDropSlotUI : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public PartType partType;

    public Color highlightBorder = Color.green;
    public Color defaultBorder = Color.white;
    public Color deadBorder = new Color(1, 1, 1, 0.2f);

    public InventoryHpBarUI hpBar;

    private Image image;

    public PartHoveredEvent mouseOver;
    public UnityEvent mouseOut;

    public Part assignedPart;

    private void Awake()
    {
        image = GetComponent<Image>();

        if (mouseOver == null)
            mouseOver = new PartHoveredEvent();

        if (mouseOut == null)
            mouseOut = new UnityEvent();
    }

    public void SetPart(Part part)
    {
        this.assignedPart = part;
        image.sprite = assignedPart.thumbnail;
        image.color = assignedPart.currentHp > 0 ? defaultBorder : deadBorder;
    }


    public void Clear()
    {
        image.sprite = null;
        this.assignedPart = null;
    }


    private static Item GetDraggedItemFromEventData(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
        {
            // não estávamos a arrastar nada
            return null;
        }

        var droppedSlot = eventData.pointerDrag.GetComponent<InventorySlotUI>();
        if (droppedSlot != null)
        {
            // estávamos a arrastar uma slot do inventário
            // (podia estar vazia ou não (null))
            return droppedSlot.item;
        }

        // estávamos a arrastar algo que não era uma slot do inventário
        return null;
    }


    public void OnDrop(PointerEventData eventData)
    {
        var part = GetDraggedItemFromEventData(eventData);
        if (part == null)
        {
            // Esta inventory slot veio vazia
            return;
        }

        ReceiveItem(part);
    }

    public void ReceiveItem(Item item)
    {
        if (item is Part part)
        {
            if (part.partType != this.partType)
            {
                // Rejeita a parte. Não encaixa aqui.
                return;
            }

            image.sprite = part.thumbnail;

            // Consume a parte
            part.Use();


            return;
        }
        
        // O item largado não é uma parte, mas vai ser aplicado numa parte
        // (caso haja uma equipada aqui)
        
        if (this.assignedPart == null)
            return;
        
        Character mainCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        foreach (Part charPart in mainCharacter.parts)
        {
            if (charPart.partType == this.partType)
            {
                
                item.ApplyOnPart(charPart);
                
                // Notifica a UI para atualiza a tooltip
                // As stats da charPart (HP ou Damage) podem ter sido alteradas
                if (assignedPart != null)
                    this.mouseOver.Invoke(this.assignedPart);
                
                return;
            }    
        }

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (this.assignedPart != null)
            this.mouseOver.Invoke(this.assignedPart);

        var item = GetDraggedItemFromEventData(eventData);
        if (item == null)
        {
            // Esta inventory slot veio vazia
            return;
        }

        if (item is Part && (item as Part).partType != this.partType)
        {
            // Estamos a arrastar uma peça que não encaixa aqui
            return;
        }

        image.color = assignedPart.currentHp > 0 ? highlightBorder : deadBorder;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.mouseOut.Invoke();
        image.color = assignedPart.currentHp > 0 ? defaultBorder : deadBorder;
    }
}