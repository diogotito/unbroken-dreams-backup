﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class InventoryHpBarUI : MonoBehaviour
{
    public Image bar;
    
    [Space(10)]
    public PartDropSlotUI slot;

    Image[] _allImages;

    void Start()
    {
        _allImages = GetComponentsInChildren<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bar && slot && slot.assignedPart)
        {
            bar.fillAmount = slot.assignedPart.currentHp / slot.assignedPart.totalHp;

            bool isPartAlive = slot.assignedPart.currentHp > 0;
            foreach (var image in _allImages)
                image.enabled = isPartAlive;
        }
    }
}