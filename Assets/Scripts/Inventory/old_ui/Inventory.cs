﻿﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Um singleton que vive no GameManager para gerir o inventário do jogador.
/// É para aqui que os Items devem Added e Removed.
/// A UI do Inventory é populada com o que estiver aqui.
/// </summary>
public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            Debug.LogWarning("Há mais de um Inventory nesta cena! É suposto só haver 1.");
        }

        instance = this;
    }

    #endregion

    public List<Item> items = new List<Item>();
    public const int MAX_CAPACITY = 12;

    public delegate void OnInventoryChanged();

    public event OnInventoryChanged onInventoryChanged;

    // Remove um item do inventário
    /// <summary>
    /// Chamem-me para pôr um novo item no inventário
    /// </summary>
    /// <returns>true se havia espaço para enfiar o novo item. false caso não caiba</returns>
    public bool Add(Item item)
    {
        if (item == null)
        {
            Debug.LogWarning("Inventário está a receber um null. Ignorar...");
            return false;
        }
        
        if (items.Count >= 12)
            return false;
        
        items.Add(item);

        onInventoryChanged?.Invoke();

        return true;
    }

    public bool Remove(Item item)
    {
        bool itemRemoved = items.Remove(item);
        
        if (itemRemoved)
            onInventoryChanged?.Invoke();

        return itemRemoved;
    }


    // Ugh
    public void ForceUpdateUI()
    {
        onInventoryChanged?.Invoke();
    }
}