﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class ItemHoveredEvent : UnityEvent<Item>
{
    // OMFG ESTES EVENTOS SAO DEFICIENTES PK EH QUE UNITYEVENT<T0> EH ABSTRACT DEVIA TER USADO DELEGATES E EVENTS DO C#
}

public class InventorySlotUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Button button;
    public Image thumbnail;
    public Canvas canvas;

    public Item item;
    Item _draggedItem;

    public ItemHoveredEvent mouseOver;
    public UnityEvent mouseOut;

    private void Awake()
    {
        if (mouseOver == null)
            mouseOver = new ItemHoveredEvent();

        if (mouseOut == null)
            mouseOut = new UnityEvent();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        // Mete a imagem arrastada em cima de tudo
        thumbnail.transform.parent = canvas.transform;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (this.item == null)
            return;

        thumbnail.transform.position = Input.mousePosition;
        _draggedItem = item;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (item == _draggedItem)
        {
            // O item não foi largado num sítio adequado.
            // Volta onde estava de uma forma smooth.
            thumbnail.transform.DOMove(transform.position, 0.5f)
                .OnComplete(() => thumbnail.transform.parent = transform);
        }
        else
        {
            // O item/parte foi usado/equipado e agora a variável `item`
            // desta slot está a apontar para o item que estava na slot a seguir
            // (ou para nenhum se esta era a última slot com um item). Confuso?
            // Aqui não quero uma translação smooth para não baralhar o jogador.
            thumbnail.transform.parent = transform;
            thumbnail.transform.localPosition = Vector3.zero;
        }
    }

    public void SetItem(Item newItem)
    {
        if (newItem == null)
        {
            Debug.LogWarning("InventorySlotUI#SetItem(null) wtf. Querias fazer ClearSlot()?");
            ClearSlot();
            return;
        }
        
        item = newItem;
        button.interactable = true;
        thumbnail.sprite = newItem.thumbnail;
        thumbnail.enabled = true;
    }

    public void ClearSlot()
    {
        item = null;
        button.interactable = false;
        thumbnail.sprite = null;
        thumbnail.enabled = false;
        mouseOver.RemoveAllListeners();
        mouseOut.RemoveAllListeners();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        this.mouseOver.Invoke(this.item);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.mouseOut.Invoke();
    }
}