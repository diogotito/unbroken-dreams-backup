﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ToggleInventoryButton : MonoBehaviour
{
    public Text label;
    public string openInventoryStr = "Open Inventory";
    public string closeInventoryStr = "Close Inventory";

    public InventoryUI inventoryUI;
    public Animator animator;

    public static ToggleInventoryButton Instance { get; private set; }
    public GameObject quitButton;

    public bool PlayerCanToggle
    {
        get => GetComponent<Button>().interactable;
        set => GetComponent<Button>().interactable = value;
    }

    private void Start()
    {
        Instance = this;

        // Start with the inventory closed
        if (inventoryUI.gameObject.activeInHierarchy)
            ToggleInventory();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Inventory") && PlayerCanToggle)
        {
            ToggleInventory();
        }
    }

    public void ToggleInventory()
    {
        if (animator.GetBool("Open"))
        {
            animator.SetBool("Open", false);
            label.text = openInventoryStr;
            quitButton.SetActive(true);
            StartCoroutine(DisableInventoryAfterTransition());
        }
        else
        {
            inventoryUI.gameObject.SetActive(true);
            inventoryUI.UpdateUI();
            label.text = closeInventoryStr;
            animator.SetBool("Open", true);
            quitButton.SetActive(false);
        }

        IEnumerator DisableInventoryAfterTransition()
        {
            yield return new WaitForSeconds(0.25f);
            // inventoryUI.gameObject.SetActive(false);
        }
    }
}
