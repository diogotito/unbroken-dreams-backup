﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] InventorySlotUI[] slotImages;
    [SerializeField] PartDropSlotUI[] partImages;

    public Text headerText;
    public Text tooltipText;

    public enum Mode
    {
        Inventory,
        Bag
    }

    public Mode uiMode = Mode.Inventory;

    private void Awake()
    {
        slotImages = GetComponentsInChildren<InventorySlotUI>();
        partImages = GetComponentsInChildren<PartDropSlotUI>();

        tooltipText.enabled = false;
        tooltipText.GetComponentInParent<Image>().enabled = false;

        if (headerText != null)
        {
            switch (uiMode)
            {
                case Mode.Bag:
                    headerText.text = "Bag";
                    break;
                case Mode.Inventory:
                    headerText.text = "Inventory";
                    break;
            }
        }
    }

    void Start()
    {
        Inventory.instance.onInventoryChanged += UpdateUI;
        Character mainCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        mainCharacter.partsModifiedCallback += UpdateUI;
        UpdateUI();
    }

    public void UpdateUI()
    {
        ClearInventory();

        int i = 0;
        foreach (var item in Inventory.instance.items)
        {
            if (uiMode == Mode.Bag && item is Part)
                continue;

            slotImages[i].SetItem(item);
            slotImages[i].mouseOver.AddListener(ShowTooltip);
            slotImages[i].mouseOut.AddListener(ClearTooltip);

            i++;
        }

        ClearParts();
        Character mainCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        SetPartSlot(mainCharacter.leftArm);
        SetPartSlot(mainCharacter.rightArm);
        SetPartSlot(mainCharacter.leftLeg);
        SetPartSlot(mainCharacter.rightLeg);
    }

    private void ClearParts()
    {
        foreach (var partImage in partImages)
        {
            partImage.Clear();
            partImage.mouseOver.RemoveAllListeners();
            partImage.mouseOut.RemoveAllListeners();
        }
    }

    private void SetPartSlot(Part mainCharacterPart)
    {
        foreach (PartDropSlotUI partSlot in partImages)
        {
            if (partSlot.partType == mainCharacterPart.partType)
            {
                Debug.Log("Bound part " + mainCharacterPart);
                partSlot.SetPart(mainCharacterPart);
                partSlot.mouseOver.AddListener(ShowTooltip);
                partSlot.mouseOut.AddListener(ClearTooltip);
                return;
            }
        }
    }

    private void ShowTooltip(Item item)
    {
        Part part = item as Part;
        if (part != null)
        {
            tooltipText.text =
                part.currentHp > 0
                    ? $"{part.partType.GetName()}\n"
                      + $"Hit points: {part.currentHp}/{part.totalHp}\n"
                      + $"Attack damage: {part.attack.dmg}\n"
                    : $"Broken {part.partType.GetName()}";
        }
        else
        {
            tooltipText.text = "Item: " + item.name;
        }

        tooltipText.enabled = true;
        tooltipText.GetComponentInParent<Image>().enabled = true;
    }

    private void ClearTooltip()
    {
        tooltipText.text = "";
        tooltipText.enabled = false;
        tooltipText.GetComponentInParent<Image>().enabled = false;
    }

    private void ClearInventory()
    {
        foreach (InventorySlotUI slot in slotImages)
        {
            slot.ClearSlot();
        }
    }

    // Esta função é para ser chamada pelo Animation Event no 1º frame
    // do clip Animations/Inventory_Transition, que é reproduzido da
    // esquerda para a direita (entrada) e da direita para a esquerda
    // (saída). No final da saída, este GameObject deve ser desativado
    // para não bloquear a interação da UI da batalha.
    public void DeactivateMyself()
    {
        Animator anim = GetComponent<Animator>();
        
        // Acabámos de reproduzir o clip da direita para a esquerda?
        if (anim && anim.GetBool("Open") == false)
        {
            gameObject.SetActive(false);
        }
    }
}