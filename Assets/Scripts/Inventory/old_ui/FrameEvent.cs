﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// A timeline do Unity não invoca eventos, mas este script sim!
/// </summary>
public class FrameEvent : MonoBehaviour
{
    public UnityEvent frameEvent;

    public void DispatchFrameEvent()
    {
        frameEvent?.Invoke();
    }
}
