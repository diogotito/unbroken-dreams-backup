﻿using System.Collections;
using UnityEngine;

public class InventoryTest : MonoBehaviour
{
    public InventoryUI inventoryUi;
    public Item[] initialItems;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AddItemsCoroutine());
    }

    IEnumerator AddItemsCoroutine()
    {
        foreach (Item initialItem in initialItems)
        {
            var newItem = Instantiate(initialItem, this.transform);
            var newPart = newItem.GetComponent<Part>();
            if (newPart != null)
            {
                newPart.enabled = false;
            }
            Inventory.instance.Add(newItem);
            Debug.Log("InventoryTest: Adding item " + initialItem);
            yield return new WaitForSeconds(0.5f);
        }
    }
}