﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState { COMBAT, EXPLORATION }

    public GameObject combatScene;
    public GameObject exploreScene;
    public GameState gameState;
    public FollowPlayer followPlayer;
    public GameObject dollPos;
    Vector3 explorePos;
    public Character player;
    public GameObject cinemachineCam;
    public SpriteRenderer battleBackground;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        StartCoroutine(startExploration());
    }


    public void startCombat(Enemy enemy)
    {
        if(this.gameState != GameState.COMBAT)
        {
            this.gameState = GameState.COMBAT;
            StartCoroutine(startCombatCoroutine(enemy));
        }
    }


    IEnumerator startCombatCoroutine(Enemy enemy)
    {
        yield return GameObject.FindGameObjectWithTag("transition").GetComponent<ImageTransition>().Cover();
        
        followPlayer.combat = true;
        explorePos = this.player.transform.localPosition;
        this.player.transform.position = dollPos.transform.position;
        exploreScene.SetActive(false);
        battleBackground.enabled = true;
        BattleManager battleManager = GameObject.FindGameObjectWithTag("battleManager").GetComponent<BattleManager>();
        player.transform.localScale = new Vector3(Mathf.Abs(player.transform.localScale.x), player.transform.localScale.y, player.transform.localScale.z);
        player.GetComponent<BoxCollider2D>().enabled = false;
        player.GetComponent<Rigidbody2D>().isKinematic = true;
        battleManager.startCombat(enemy);
        
        yield return GameObject.FindGameObjectWithTag("transition").GetComponent<ImageTransition>().Reveal();
        cinemachineCam.GetComponent<Cinemachine.CinemachineVirtualCamera>().enabled = false;
        cinemachineCam.GetComponent<Cinemachine.CinemachineVirtualCamera>().enabled = true;
    }


    public void endCombat()
    {
        StartCoroutine(EndCombatCoroutine());
    }
    
    public IEnumerator EndCombatCoroutine()
    {
        yield return GameObject.FindGameObjectWithTag("transition").GetComponent<ImageTransition>().Cover();
        
        GameObject.FindGameObjectWithTag("Player").transform.rotation = Quaternion.identity;
        followPlayer.combat = false;
        StartCoroutine(startExploration());
    }


    IEnumerator startExploration()
    {
        gameState = GameState.EXPLORATION;
        exploreScene.SetActive(true);
        battleBackground.enabled = false;
        player.transform.localPosition = explorePos; //+ Vector3.up * 100.0f;
        player.GetComponent<BoxCollider2D>().enabled = true;
        player.GetComponent<Rigidbody2D>().isKinematic = false;
        player.transform.localScale = new Vector3(Mathf.Abs(player.transform.localScale.x), player.transform.localScale.y, player.transform.localScale.z);
        GameObject.FindGameObjectWithTag("Player").transform.rotation = Quaternion.identity;
        /*
        for (int i = 0; i < 120; i++)
        {
            player.transform.localPosition = explorePos + Vector3.up * 100.0f;
            if (i % 20 == 0) Debug.Log("hackety hack hack " + (i/20 + 1) + "/6");
            yield return null;
        }
        */
        //StartCoroutine(startCombat());
        
        yield return GameObject.FindGameObjectWithTag("transition").GetComponent<ImageTransition>().Reveal();
    }
}
