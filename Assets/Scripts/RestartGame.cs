﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public string sceneName = "PedroScene";
    
    public TextMeshProUGUI restartText;

    public string[] animatedLabel = new[]
    {
        " Restart",
        " <u>R</u>estart",
        " <u>Re</u>start",
        " <u>Res</u>tart",
        " <u>Rest</u>art",
        " <u>Resta</u>rt",
        " <u>Restar</u>t",
        " <u>Restart</u>"
    };

    public string[] restartingLabels = new[] {
        "Restarting",
        "Restarting.",
        "Restarting..",
        "Restarting..."
    };

    private IEnumerator HighlightLabel()
    {
        foreach (var label in animatedLabel)
        {
            restartText.text = label;
            yield return null;
        }
    }

    private IEnumerator UnhighlightLabel()
    {
        for (var i = animatedLabel.Length - 1; i >= 0; i--)
        {
            restartText.text = animatedLabel[i];
            yield return null;
        }
    }

    private IEnumerator LoadGameScene()
    {
        var loading = SceneManager.LoadSceneAsync(sceneName);
        
        var i = 0;
        while (!loading.isDone)
        {
            i = (i + 1) % restartingLabels.Length;
            restartText.text = restartingLabels[i];
            yield return null;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        StartCoroutine(HighlightLabel());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StartCoroutine(UnhighlightLabel());
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (sceneName == "")
        {
            Application.Quit();
            return;
        }
        StartCoroutine(LoadGameScene());
    }
}
