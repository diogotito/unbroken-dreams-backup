﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour
{
    /*Light testeLight;

    void Start()
    {
        testeLight = GetComponent<Light>();
        StartCoroutine(Flashing());
    }

    IEnumerator Flashing()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.5f);
            testeLight.enabled = !testeLight.enabled;
        }
    }*/

    public GameObject Light_gm;
    public float timer;

    private Light gm_light;

    void Start()
    {
        gm_light = Light_gm.GetComponent<Light>();
        StartCoroutine(FlickeringLights());
    }

    IEnumerator FlickeringLights()
    {
        while (true)
        {
            gm_light.enabled = true;
            timer = Random.Range(0.1f,1);
            yield return new WaitForSeconds(timer);
            gm_light.enabled = false;
            timer = Random.Range(0.1f,1);
            yield return new WaitForSeconds(timer);
        }
    }
}
