﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject objectToFollow;
    public GameObject objectToFollow2;
    public GameObject Character;
    public Vector3 offset;
    public GameObject specialCam;
    public bool combat;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(combat)
        {
           Vector3 newPos = new Vector3( (objectToFollow2.transform.position.x - objectToFollow.transform.position.x) / 2, 
                                            (objectToFollow2.transform.position.y - objectToFollow.transform.position.y) / 2, 
                                            0) + objectToFollow.transform.position;
            newPos.z = transform.position.z;
            transform.position = newPos;
            specialCam.SetActive(false);
        }
        else { 
            transform.position = new Vector3(Character.transform.position.x, Character.transform.position.y + offset.y, transform.position.z);
            specialCam.SetActive(true);
        }
    }


}
