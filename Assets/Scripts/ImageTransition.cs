﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageTransition : MonoBehaviour
{
    public Image cover;

    void Start()
    {
        var chosenFillMethod = cover.fillMethod;
        cover.fillMethod = Image.FillMethod.Vertical;
        cover.fillOrigin = (int) Image.OriginVertical.Top;
        cover.fillAmount = 1;
        cover.DOFillAmount(0, 0.5f)
            .OnComplete(() => cover.fillMethod = chosenFillMethod);
    }

    public CustomYieldInstruction Cover(float duration = 1)
    {
        if (DOTween.IsTweening(cover))
            return null;
        
        cover.fillClockwise = true;
        return cover.DOFillAmount(1, duration)
            .WaitForCompletion(true);
    }

    public IEnumerator Reveal(float duration = 1)
    {
        if (DOTween.IsTweening(cover))
            return null;
        
        cover.fillClockwise = false;
        return cover.DOFillAmount(0, duration)
            .WaitForCompletion(true);
    }

    public void ExitScene(string nextScene)
    {
        cover.fillMethod = Image.FillMethod.Vertical;
        cover.fillOrigin = (int) Image.OriginVertical.Top;
        cover.DOFillAmount(1, 2)
            .OnComplete(() => SceneManager.LoadScene(nextScene));
    }
}