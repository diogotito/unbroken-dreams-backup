﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Character : MonoBehaviour
{
    public float speed;
    public float brokeness;
    public List<Part> parts;
    public Animator animator;

    public GameObject rightArmSlot;
    public GameObject leftArmSlot;
    public GameObject rightLegSlot;
    public GameObject leftLegSlot;

    public Part leftArm;
    public Part rightArm;
    public Part leftLeg;
    public Part rightLeg;

    public Part partToCredit;

    public HpBarSprite leftArmHp;
    public HpBarSprite rightArmHp;
    public HpBarSprite leftLegHp;
    public HpBarSprite rightLegHp;

    public Part testPart;
    public Part lastPart;
    private BattleManager battleManager;
    private GameManager gameManager;

    private Part targetPart;
    private Part partAttacking;
    private Character targetEnemy;

    public Enemy robot;
    public Enemy car;
    public Enemy dino;
    public Enemy bear;

    public delegate void PartsModified();

    public event PartsModified partsModifiedCallback;

    private void Awake()
    {
        parts = new List<Part> {leftArm, rightArm, leftLeg, rightLeg};
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        this.battleManager = GameObject.FindGameObjectWithTag("battleManager").GetComponent<BattleManager>();
        this.gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
    }

    public void takeDamage(Part targetPart, Part partAttacking)
    {
        float successChance = partAttacking.currentHp / partAttacking.totalHp;
        successChance = Mathf.Max(0.7f, successChance);

        float r = Random.Range(0.0f, 1.0f);
        float dmg = 0;

        if (targetPart is PartDino)
        {
            PartDino tmp = (PartDino) targetPart;
            tmp.increaseDamage();
        }

        if (partAttacking is PartRobot && ((PartRobot)partAttacking).doAreaDmg && (partAttacking.partType == PartType.LeftArm || partAttacking.partType == PartType.RightArm))
        {
            PartRobot partR = (PartRobot) partAttacking;

            int k = 1;
            //if (partR.getHpRatio() > partR.ratioThreshold)
            //{

                if (partR.character is Enemy)
                {
                    foreach (Part part in this.battleManager.doll.parts)
                    {
                        if(part is PartDino)
                        {
                            PartDino tmp = (PartDino)part;
                            tmp.increaseDamage();
                        }

                        StartCoroutine(damageEffect(partR.getDmg(), part.transform.position, partAttacking, 0.1f * k));
                        part.currentHp -= partR.getDmg();
                        k++;
                    }
                }
                else
                {
                    foreach (Part part in this.battleManager.enemy.parts)
                    {
                        if (part is PartDino)
                        {
                            PartDino tmp = (PartDino)part;
                            tmp.increaseDamage();
                        }

                        StartCoroutine(damageEffect(partR.getDmg(), part.transform.position, partAttacking, 0.1f * k));
                        part.currentHp -= partR.getDmg();
                        k++;
                    }
                }
            //}
            /*
            else
            {
                if (partR.character is Enemy)
                {
                    foreach (Part part in this.battleManager.enemy.parts)
                    {
                        if (part is PartDino)
                        {
                            PartDino tmp = (PartDino) part;
                            tmp.increaseDamage();
                        }

                        StartCoroutine(damageEffect(partR.getDmg(), part.transform.position, partAttacking, 0.1f * k));
                        part.currentHp -= partR.getOverheatDmg();
                        k++;
                    }
                }
                else
                {
                    foreach (Part part in this.battleManager.doll.parts)
                    {
                        if (part is PartDino)
                        {
                            PartDino tmp = (PartDino)part;
                            tmp.increaseDamage();
                        }

                        StartCoroutine(damageEffect(partR.getDmg(), part.transform.position, partAttacking, 0.1f * k));
                        part.currentHp -= partR.getOverheatDmg();
                        k++;
                    }
                }
            }
            */
        }

        partAttacking.currentHp -= partAttacking.attack.brokeness;

        if (r < successChance)
            dmg = partAttacking.attack.dmg;

        int i = (int) partAttacking.attackType;
        int j = (int) targetPart.attackType;

        targetPart.currentHp = targetPart.currentHp - dmg;
        targetPart.takeDamage(dmg);

        GameObject go = Instantiate(partAttacking.damageTextParticleSystem, targetPart.transform.position, Quaternion.identity);
        if (dmg <= 0)
        {
            go.GetComponent<TextMeshPro>().text = "MISS";
        }
        else
        {
            go.GetComponent<TextMeshPro>().text = "" + dmg;
        }
        go.SetActive(true);

        go = Instantiate(partAttacking.damageParticleSystem, targetPart.transform.position, Quaternion.identity);
        go.SetActive(true);


        if (targetPart.currentHp <= 0)
        {
            parts.Remove(targetPart);
            //Destroy(targetPart.gameObject);
            targetPart.gameObject.SetActive(false);

            partsModifiedCallback?.Invoke();
        }

        if (partAttacking.currentHp <= 0)
        {

            if(partAttacking.character is Enemy)
            {
                this.battleManager.enemy.parts.Remove(partAttacking);
            }
            else
            {
                this.battleManager.doll.parts.Remove(partAttacking);
            }
            //Destroy(targetPart.gameObject);
            partAttacking.gameObject.SetActive(false);

            partsModifiedCallback?.Invoke();
        }
    }


    public IEnumerator damageEffect(float dmg, Vector3 targetPartPos, Part partAttacking, float time)
    {
        yield return new WaitForSeconds(time);

        GameObject parent = new GameObject();
        parent.transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
        GameObject go = Instantiate(partAttacking.damageTextParticleSystem, targetPartPos, Quaternion.identity);
        go.transform.parent = parent.transform;
        if (dmg <= 0)
        {
            go.GetComponent<TextMeshPro>().text = "MISS";
        }
        else
        {
            go.GetComponent<TextMeshPro>().text = "" + dmg;
        }
        go.SetActive(true);

        go = Instantiate(partAttacking.damageParticleSystem, targetPartPos, Quaternion.identity);
        go.SetActive(true);
    }


    public void prepareAttack(Part targetPart, Part partAttacking, Character character)
    {
        this.targetPart = targetPart;
        this.partAttacking = partAttacking;
        this.targetEnemy = character;
    }


    public void useAttack()
    {
        this.targetEnemy.takeDamage(this.targetPart, this.partAttacking);
        this.battleManager.nextTurn();
    }


    public void setPart(Part attatched, Part removed)
    {
        parts.Remove(removed);
        parts.Add(attatched);
    }


    public void setPart(Part newPart)
    {
        if (newPart.partType == PartType.LeftArm)
        {
            if (leftArm != null)
            {
                //leftArm.enabled = false;
                leftArm.gameObject.SetActive(false);
                //Destroy(leftArm.gameObject);
                parts.Remove(leftArm);
            }

            Part newPartInst = Instantiate(newPart, leftArmSlot.transform);
            newPartInst.enabled = true;
            newPartInst.character = this;
            parts.Add(newPartInst);
            leftArmHp.part = newPartInst;
            leftArm = newPartInst;

            partsModifiedCallback?.Invoke();
        }
        else if (newPart.partType == PartType.RightArm)
        {
            if (rightArm != null) {
                //rightArm.enabled = false;
                rightArm.gameObject.SetActive(false);
                //Destroy(rightArm.gameObject);
                parts.Remove(rightArm);
            }

            Part newPartInst = Instantiate(newPart, rightArmSlot.transform);
            newPartInst.enabled = true;
            newPartInst.character = this;
            parts.Add(newPartInst);
            rightArmHp.part = newPartInst;
            rightArm = newPartInst;

            partsModifiedCallback?.Invoke();
        }
        else if (newPart.partType == PartType.LeftLeg)
        {
            if (leftLeg != null)
            {
                //leftLeg.enabled = false;
                leftLeg.gameObject.SetActive(false);
                //Destroy(leftLeg.gameObject);
                parts.Remove(leftLeg);
            }

            Part newPartInst = Instantiate(newPart, leftLegSlot.transform);
            newPartInst.enabled = true;
            newPartInst.character = this;
            parts.Add(newPartInst);
            leftLegHp.part = newPartInst;
            leftLeg = newPartInst;

            partsModifiedCallback?.Invoke();
        }
        else if (newPart.partType == PartType.RightLeg)
        {
            if (rightLeg != null) {
                //rightLeg.enabled = false;
                rightLeg.gameObject.SetActive(false);
                //Destroy(rightLeg.gameObject);
                parts.Remove(rightLeg);
            }

            Part newPartInst = Instantiate(newPart, rightLegSlot.transform);
            newPartInst.enabled = true;
            newPartInst.character = this;
            parts.Add(newPartInst);
            rightLegHp.part = newPartInst;
            rightLeg = newPartInst;

            partsModifiedCallback?.Invoke();
        }
    }


    public bool isDead()
    {
        return parts.Count <= 0;
    }

    /*
    public void battleDino()
    {
        this.gameManager.startCombat(dino);
    }
    public void battleCar()
    {
        this.gameManager.startCombat(car);
    }
    public void battleRobot()
    {
        this.gameManager.startCombat(robot);
    }
    public void battleBear()
    {
        this.gameManager.startCombat(bear);
    }
    */
}