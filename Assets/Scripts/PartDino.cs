﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartDino : Part
{
    [Header("Dinosaur parameters ----- Grrrrr!")]
    public Color[] colors = {Color.green, Color.yellow, new Color(0.2F, 0.3F, 0.4F), Color.red };
    public float[] dmgExtra = { 5.0f, 10.0f, 10.0f, 10.0f };
    public int currentState;
    public float initialDamage;

    public void Start()
    {
        base.Start();
        currentState = 0;
    }

    public void increaseDamage()
    {
        currentState++;
        currentState = Mathf.Min(currentState, dmgExtra.Length-1);
        this.attack.dmg = this.attack.dmg + dmgExtra[currentState];
        this.spriteRenderer.color = colors[currentState];
    }

    public override IEnumerator resetColor(float wait = 0)
    {
        yield return new WaitForSeconds(wait);
        this.spriteRenderer.color = colors[currentState];
    }

    public override void startCombat()
    {
        base.startCombat();
        this.currentState = 0;
        initialDamage = this.attack.dmg;

        if (this.spriteRenderer)
            this.spriteRenderer.color = Color.white;
    }

    public override void endCombat()
    {
        base.endCombat();
        this.attack.dmg = initialDamage;

        if (this.spriteRenderer)
            this.spriteRenderer.color = Color.white;
    }
}
