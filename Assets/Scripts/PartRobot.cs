﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartRobot : Part
{
    [Header("Robot parameters ----- *BEEP* *BOOP*")]
    public float ratioThreshold = 0.4f;
    public float dotDamage = 0.2f;
    public float overheatDamage = 0.1f;
    public bool doAreaDmg;

    public float getDmg()
    {
        return dotDamage;
    }

    public float getOverheatDmg()
    {
        return this.attack.dmg * overheatDamage;
    }
}
