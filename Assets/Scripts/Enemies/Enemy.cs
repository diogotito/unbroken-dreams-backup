﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTargetPair
{
    public Attack attack;
    public Part target;
    public Part attackingPart;

    public AttackTargetPair(Attack attack, Part target)
    {
        this.attack = attack;
        this.target = target;
    }
}

public class Enemy : Character
{
    public float prioPer = 0.32f;
    public PartType partTypeToPrio1;
    public PartType partTypeToPrio2;
    public GameObject prioAttackText;
    public Vector3 baloonOffset = new Vector3(0f,1f,0f);
    private int usedPrio = 0;
    private bool showEach = true;
    public bool bbge = false;

    public AttackTargetPair pickRandomAttack(List<Part> parts)
    {
        int r = Random.RandomRange(0, this.parts.Count);
        int r2 = Random.RandomRange(0, parts.Count);

        if(usedPrio < 2 && !showEach)
        {
            int prio1 = -1;
            int prio2 = -1;

            for (int i = 0; i < this.parts.Count; i++)
            {
                if (this.parts[i].partType == partTypeToPrio1)
                    prio1 = i;
                else if(this.parts[i].partType == partTypeToPrio2)
                {
                    prio2 = i;
                }
            }

            float t = Random.Range(0f, 1f);
            if(t > 0.5f && prio1 != -1)
            {
                r = prio1;
            }
            else if(t < 0.5 && prio2 != -1)
            {
                r = prio2;
            }

            usedPrio++;
        }
        else
        {
            for (int i = 0; i < this.parts.Count; i++)
            {
                if (!(this.parts[i].partType == partTypeToPrio1) && !(this.parts[i].partType == partTypeToPrio2))
                    r = i;
            }
            usedPrio = 0;
            showEach = false;
        }

        if (this.parts[r].partType == partTypeToPrio1 || this.parts[r].partType == partTypeToPrio2)
        {
            Instantiate(prioAttackText, this.transform.position + baloonOffset, Quaternion.identity);
        }

        AttackTargetPair attackTargetPair = new AttackTargetPair(this.parts[r].attack, parts[r2]);
        attackTargetPair.attackingPart = this.parts[r];
        return attackTargetPair;
    }
}
