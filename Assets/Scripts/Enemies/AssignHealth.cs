﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignHealth : MonoBehaviour
{
    public HpBar leftArm;
    public HpBar rightArm;
    public HpBar leftLeg;
    public HpBar rightLeg;

    public void assignEnemyHealth(Enemy enemy)
    {
        leftArm.part = enemy.leftArm;
        rightArm.part = enemy.rightArm;
        leftLeg.part = enemy.leftLeg;
        rightLeg.part = enemy.rightLeg;
    }
}
