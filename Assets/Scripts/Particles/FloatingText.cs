﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float destroyTime = 1.0f;
    public Vector3 Offset = new Vector3(0, 2, 0);
    public Vector3 RandomItensity = new Vector3(0.5f, 0, 0);
    public float speed = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destroyTime);

        transform.localPosition += Offset;
        transform.localPosition += new Vector3(Random.Range(-RandomItensity.x, RandomItensity.y),
                                                Random.Range(-RandomItensity.x, RandomItensity.y),
                                                Random.Range(-RandomItensity.z, RandomItensity.z));
    }

    private void Update()
    {
        transform.localPosition += Vector3.up * speed;
    }
}
