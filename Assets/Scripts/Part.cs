﻿using System;
using System.Collections;
using UnityEngine;

public enum PartType {LeftArm, RightArm, RightLeg, LeftLeg, Torso};
public enum AttackType { Tecido = 0, Plastico = 1, Velocidade = 2, Fogo = 3 };

public static class PartTypeNames
{
    public static string GetName(this PartType partType)
    {
        switch (partType)
        {
            case PartType.LeftArm:  return "Left Arm";
            case PartType.RightArm: return "Right Arm";
            case PartType.RightLeg: return "Right Leg";
            case PartType.LeftLeg:  return "Left Leg";
            case PartType.Torso:    return "Torso";
            default:                return "???";
        }
    }
}

public class Part : Item
{
    public float totalHp = 100.0f;
    public float currentHp = 100.0f;
    public Attack attack;
    public PartType partType;
    public SpriteRenderer spriteRenderer;
    public Character character;
    public string attackAnimName;

    private BattleManager battleManager;
    private ParticleFactory particleFactory;
    public static float[,] attackTable = new float[,] { { 1, 1, 1.25f, 1 }, { 1, 1, 1, 0.75f }, { 0.75f, 1, 1, 1.25f }, { 1.25f, 1.25f, 0.75f, 0.75f } };
    public AttackType attackType = AttackType.Tecido;
    public string description;
    public GameObject damageParticleSystem;
    public GameObject damageTextParticleSystem;

    [Header("UI Display")]
    public string displayName = "doll";
    public Sprite cardFace;

    // Attack slots will subscribe to this so that they also flash when this part takes damage
    public event Action OnTakeDamage;

    // Start is called before the first frame update
    protected void Start()
    {
        this.currentHp = this.totalHp;
        this.battleManager = GameObject.FindGameObjectWithTag("battleManager").GetComponent<BattleManager>();
        this.particleFactory = GameObject.FindGameObjectWithTag("particleFactory").GetComponent<ParticleFactory>();

        if(!this.damageParticleSystem)
        {
            this.damageParticleSystem = this.particleFactory.hitEffect;
        }

        if (!this.damageTextParticleSystem)
        {
            this.damageTextParticleSystem = this.particleFactory.floatingText;
        }

        if (!this.character) {
            this.character = transform.parent.parent.GetComponent<Character>();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if(!this.spriteRenderer)
            this.spriteRenderer = GetComponent<SpriteRenderer>();
    }


    private void OnMouseUp()
    {
        this.battleManager.attack(this);
    }


    public void playAnimation()
    {
        character.animator.Play(attackAnimName);
    }


    public string getPartTypeStr()
    {
        if(this.partType == PartType.LeftArm)
        {
            return "Left Arm";
        }
        else if(this.partType == PartType.RightArm)
        {
            return "Right Arm";
        }
        else if(this.partType == PartType.RightLeg)
        {
            return "Right Leg";
        }
        else if(this.partType == PartType.LeftLeg)
        {
            return "Left Leg";
        }

        return "Crink Cronk your code is wrong";
    }


    public virtual void startCombat() {}

    public virtual void endCombat() {
        this.spriteRenderer.color = Color.white;
    }


    public void takeDamage(float damage) {
        OnTakeDamage?.Invoke();
        this.spriteRenderer.color = Color.red;
        StartCoroutine(resetColor(0.1f));
    }

    public void GetHighlighted()
    {
        this.spriteRenderer.color = new Color32(0xFF, 0xF7, 0x5F, 0xFF);
    }

    public virtual IEnumerator resetColor(float wait = 0)
    {
        yield return new WaitForSeconds(wait);
        this.spriteRenderer.color = Color.white;
    }

    public override void Use()
    {
        base.Use();
        Character mainCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
        mainCharacter.setPart(this);
    }


    public float getHpRatio()
    {
        return this.currentHp / this.totalHp;
    }
}
