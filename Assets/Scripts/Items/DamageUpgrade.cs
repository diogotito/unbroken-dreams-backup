﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpgrade : Item
{
    public float amountDamageIncreasePart = 20.0f;
    public float amountDamageIncreasePartRobot = 10.0f;
    public float amountDamageIncreasePartRobotDot = 5.0f;
    public float amountDamageIncreasePartDino = 10.0f;
    public float amountDamageIncreasePartDinoBuff = 5.0f;

    public override void ApplyOnPart(Part partToIncreaseDmg)
    {
        if(partToIncreaseDmg is PartRobot)
        {
            PartRobot tmp = (PartRobot)partToIncreaseDmg;
            //partToIncreaseDmg.attack.dmg = Mathf.FloorToInt(amountDamageIncreasePartRobot + partToIncreaseDmg.attack.dmg);
            tmp.dotDamage = Mathf.FloorToInt(tmp.dotDamage + amountDamageIncreasePartRobotDot);
        }
        else if(partToIncreaseDmg is PartDino)
        {
            PartDino tmp = (PartDino)partToIncreaseDmg;
            partToIncreaseDmg.attack.dmg = Mathf.FloorToInt(amountDamageIncreasePartDino + partToIncreaseDmg.attack.dmg);
            for(int i = 0; i < tmp.dmgExtra.Length; i++)
            {
                tmp.dmgExtra[i] += amountDamageIncreasePartDinoBuff;
            }
        }
        else
        {
            partToIncreaseDmg.attack.dmg = Mathf.FloorToInt(amountDamageIncreasePart + partToIncreaseDmg.attack.dmg);
        }

        base.Use();
    }
}
