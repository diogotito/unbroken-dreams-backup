﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingItem : Item
{
    public int amountToHeal = 10;

    public override void ApplyOnPart(Part partToHeal)
    {
        Debug.Log($"VOU CURAR {partToHeal.name} COM {amountToHeal} DE HP!");
        partToHeal.currentHp += amountToHeal;
        partToHeal.currentHp = Mathf.Min(partToHeal.currentHp, partToHeal.totalHp);
        base.Use();
    }
}
