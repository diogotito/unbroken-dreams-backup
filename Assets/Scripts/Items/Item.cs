﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Sprite thumbnail;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Use()
    {
        Inventory.instance.Remove(this);
    }

    public virtual void ApplyOnPart(Part part)
    {
        Inventory.instance.Remove(this);
    }
}
