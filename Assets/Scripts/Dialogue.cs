﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    private bool canTalk = false;
    public GameObject bubble_talk;
    private GameObject dialoge_man;
    private GameObject bubble_to_delete;
    private string[] names_to_talk = { "BBEG_img", "Robot_img", "Dino_img", "Car_img" };
    private string name_to_talk = "";


    private void Start()
    {
        dialoge_man = GameObject.Find("DialogueManager");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (String namez in names_to_talk)
        {
            if (collision.transform.name == namez)
            {
                canTalk = true;
                bubble_to_delete = Instantiate(bubble_talk, collision.transform);
                bubble_to_delete.transform.position = bubble_to_delete.transform.position + new Vector3(0, 1, 0);
                name_to_talk = collision.transform.name;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        foreach (String namez in names_to_talk)
        {
            if (collision.transform.name == namez)
            {
                canTalk = false;
                Destroy(bubble_to_delete);
                name_to_talk = "";
            }
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (canTalk)
            {
                canTalk = false;
                dialoge_man.GetComponent<DialogueManager>().start_conversation(name_to_talk);
            }
        }
    }
}