﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBaloon : MonoBehaviour
{
    public Transform balloonGraphic;
    public float maxSpeed = 6.0f;
    public TextMesh textMesh;
    public SpriteRenderer gear;

    private Vector3 _balloonOffset;

    public float waveHeight = 1f;

    private void Awake()
    {
        _balloonOffset = balloonGraphic.localPosition;
        Show();
    }

    // Start is called before the first frame update
    private void Update()
    {
        // Wave
        if (gear.enabled)
        {
            balloonGraphic.localPosition =
                Vector3.up * waveHeight * 0.5f * (0.5f + Mathf.Sin(Time.time * maxSpeed)) + _balloonOffset;
        }
    }

    public void SetText(string text)
    {
        textMesh.text = text;
        HideGear();
    }

    public void ShowGear()
    {
        gear.enabled = true;
        textMesh.gameObject.SetActive(false);
    }

    public void HideGear()
    {
        gear.enabled = false;
        textMesh.gameObject.SetActive(true);
    }

    public void Hide()
    {
        balloonGraphic.gameObject.SetActive(false);
        gear.enabled = false;
        textMesh.gameObject.SetActive(false);
    }

    public void Show()
    {
        balloonGraphic.gameObject.SetActive(true);
        ShowGear();
    }
}