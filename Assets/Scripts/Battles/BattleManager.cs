﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour
{
    enum BattleState { PLAYERTURN, AITURN }

    public Character doll;
    public Enemy enemy;
    public bool debug = true;
    public bool firstRun = true;

    public GameObject[] attackSlots;
    public GameObject[] initialSlots;
    public AttackSlot selectedAttackSlot;

    public GameManager gameManager;

    private BattleState battleState;
    public GameObject enemySlot;
    public Enemy enemyToSpawn;
    public AssignHealth assignHealth;
    public GameObject battleCanvas;
    public GameObject healthBars;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
    }


    // Update is called once per frame
    void Update()
    {
    }


    public void startCombat(Enemy enemy)
    {
        battleCanvas.SetActive(true);
        Enemy enem = Instantiate(enemy, enemySlot.transform);
        
        foreach (var sprite in enem.GetComponentsInChildren<SpriteRenderer>())
            sprite.sortingLayerName = "Battle";
        
        this.enemy = enem;
        doll.animator.enabled = true;
        healthBars.SetActive(true);
        doll.GetComponent<MovePlayer>().enabled = false;
        assignHealth.assignEnemyHealth(this.enemy);
        assignHealth.gameObject.SetActive(true);

        foreach (Part part in this.doll.parts)
        {
            part.startCombat();
        }
        foreach(Part part in this.enemy.parts)
        {
            part.startCombat();
        }

        StartCoroutine(setupPlayerTurn());
    }


    public void setAttacks()
    {
        if (firstRun)
        {
            initialSlots = GameObject.FindGameObjectsWithTag("attackSlot");
            firstRun = false;
        }
        
        for (int i = 0; i < initialSlots.Length; i++)
        {
            initialSlots[i].SetActive(true);
        }

        for (int i = 0; i < initialSlots.Length; i++)
        {
            if (doll.parts.Count > i)
            {
                initialSlots[i].GetComponent<AttackSlot>().SetPart(doll.parts[i]);
            }
            else
            {
                initialSlots[i].SetActive(false);
            }
        }
    }


    IEnumerator setupPlayerTurn()
    {
        if (this.attackSlots != null)
        {
            this.attackSlots = GameObject.FindGameObjectsWithTag("attackSlot");
            foreach (var slot in attackSlots)
            {
                slot.GetComponent<AttackSlot>().WaitingForAi = false;
                slot.GetComponent<Button>().interactable = true;
            }
        }
        
        ToggleInventoryButton.Instance.PlayerCanToggle = true;
        
        setAttacks();
        yield return new WaitForSeconds(1.0f);
        battleState = BattleState.PLAYERTURN;
    }


    public void selectAttack(AttackSlot attackSlot)
    {
        if (this.battleState != BattleState.PLAYERTURN)
            return;

        if (this.selectedAttackSlot != null)
        {
            this.selectedAttackSlot.GetComponent<Button>().interactable = true;
            this.selectedAttackSlot.ResetPartColor();
        }

        this.selectedAttackSlot = attackSlot;
        this.selectedAttackSlot.GetComponent<Button>().interactable = false;
        this.selectedAttackSlot.HighlightPart();
    }


    public void attack(Part part)
    {
        if (this.battleState != BattleState.PLAYERTURN || !this.selectedAttackSlot)
            return;
        
        foreach (var slot in attackSlots)
            if (slot.GetComponent<Button>().interactable)
                slot.GetComponent<AttackSlot>().WaitingForAi = true;
            else
                slot.GetComponent<AttackSlot>().ResetPartColor();
        
        ToggleInventoryButton.Instance.PlayerCanToggle = false;

        if(!debug)
            this.selectedAttackSlot.part.playAnimation();

        this.doll.prepareAttack(part, this.selectedAttackSlot.part, this.enemy);

        foreach(var slot in this.attackSlots) {
            if(slot.activeSelf) {
                slot.GetComponent<Button>().interactable = true;
                slot.GetComponent<AttackSlot>().ResetPartColor();
            }
        }

        this.selectedAttackSlot = null;

        if (debug)
            this.doll.useAttack();
    }


    public void nextTurn()
    {
        if(this.battleState == BattleState.PLAYERTURN) {
            if(doll.isDead())
            {
                lose();
            }
            else if (enemy.isDead())
            {
                Destroy(enemy.gameObject);
                win();
            }
            else
            {
                StartCoroutine(setupEnemyTurn());
            }
        }

        else if(this.battleState == BattleState.AITURN)
        {
            if (doll.isDead())
            {
                lose();
            }
            else if (enemy.isDead())
            {
                Destroy(enemy.gameObject);
                win();
            }
            else
            {
                StartCoroutine(setupPlayerTurn());
            }
        }
    }


    IEnumerator setupEnemyTurn()
    {
        this.battleState = BattleState.AITURN;

        if(!debug)
            yield return new WaitForSeconds(1.0f);
        else
        {
            yield return new WaitForSeconds(0.1f);
        }

        AttackTargetPair attackTargetPair = enemy.pickRandomAttack(doll.parts);

        if(!debug)
            attackTargetPair.attackingPart.playAnimation();

        this.enemy.prepareAttack(attackTargetPair.target, attackTargetPair.attackingPart, this.doll);

        if (debug)
            this.enemy.useAttack();
    }


    public void endCombat()
    {
        foreach (Part part in this.doll.parts)
        {
            part.endCombat();
        }

        ToggleInventoryButton.Instance.PlayerCanToggle = true;
        doll.animator.Rebind();
        doll.animator.enabled = false;
        doll.GetComponent<MovePlayer>().enabled = true;
        battleCanvas.SetActive(false);
        healthBars.SetActive(false);
    }


    public void win()
    {
        Inventory.instance.Add(enemy.partToCredit);
        endCombat();
        this.gameManager.endCombat();

        if (this.enemy.bbge)
            SceneManager.LoadScene("BARBIE");
    }


    public void lose()
    {
        GameObject.FindGameObjectWithTag("transition").GetComponent<ImageTransition>().ExitScene("LoseScene");
    }

}
