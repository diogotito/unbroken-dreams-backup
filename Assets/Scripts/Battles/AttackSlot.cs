﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class AttackSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Attack attack;
    public Part part;
    Image image;
    public Animator animator;
    TextMeshProUGUI partUsed;
    TextMeshProUGUI dmgText;
    TextMeshProUGUI selfDmgText;
    TextMeshProUGUI hitChance;
    TextMeshProUGUI textdescription;

    bool _waitingForAi = false;
    public bool WaitingForAi
    {
        get => _waitingForAi;
        set
        {
            _waitingForAi = value;
            image.CrossFadeAlpha(_waitingForAi ? 0.5f : 1, 0.2f, false);
        }
    }

    public void Awake()
    {
        image = GetComponent<Image>();
        animator = GetComponent<Animator>();

        partUsed = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        dmgText = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        selfDmgText = transform.GetChild(3).GetComponent<TextMeshProUGUI>();
        hitChance = transform.GetChild(4).GetComponent<TextMeshProUGUI>();
        textdescription = transform.GetChild(5).GetComponent<TextMeshProUGUI>();

        // Setup button click
        var battleManager = GameObject.FindGameObjectWithTag("battleManager").GetComponent<BattleManager>();
        GetComponent<Button>().onClick.AddListener(() => battleManager.selectAttack(this));
    }


    public void SetPart(Part newPart)
    {
        this.part = newPart;
        this.image.sprite = newPart.cardFace;
        this.partUsed.text = newPart.partType.GetName();
        this.attack = newPart.attack;
        this.dmgText.text = "" + newPart.attack.dmg;
        
        newPart.OnTakeDamage += () => animator.SetTrigger("Damage");
    }

    public void HighlightPart() => this.part.GetHighlighted();
    public void ResetPartColor() => StartCoroutine(this.part.resetColor());

    public void Update()
    {
        if (part)
        {
            partUsed.text = part.getPartTypeStr() + " (" + this.part.displayName + ")";
            dmgText.text = "Damage: " + part.attack.dmg;
            selfDmgText.text = "Self Damage: " + part.attack.brokeness;
            hitChance.text = "Hit Chance: " + (int) (Mathf.Max(0.7f, part.getHpRatio()) * 100.0f) + "%";
            textdescription.text = part.description;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!_waitingForAi)
            HighlightPart();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (GetComponent<Button>().interactable)
            ResetPartColor();
    }
}